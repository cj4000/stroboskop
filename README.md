# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://cj4000@bitbucket.org/cj4000/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/cj4000/stroboskop/commits/51d61e27593f69570f164422097b9203662e42e1

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/cj4000/stroboskop/commits/39ee2233ed78d2dc21eebee511a2800e7364d9f6

Naloga 6.3.2:
https://bitbucket.org/cj4000/stroboskop/commits/a3a015f89c4e2662a95127e56b51d6d8c4144d79

Naloga 6.3.3:
https://bitbucket.org/cj4000/stroboskop/commits/fdcbbafe6f0c2501f2dc55d8d5565cd4a3c86eee

Naloga 6.3.4:
https://bitbucket.org/cj4000/stroboskop/commits/5e3feffd28abcaf9494bdbb26a6468e16f0585bc

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/cj4000/stroboskop/commits/c58e020fddf47b57c9483a1ab2592e2e6883aebc

Naloga 6.4.2:
https://bitbucket.org/cj4000/stroboskop/commits/a80d0b426a3478a6af250b4f2dda792068b298a3

Naloga 6.4.3:
https://bitbucket.org/cj4000/stroboskop/commits/b2ccbae26fcd159becd4fe0977cf6aa742756114

Naloga 6.4.4:
https://bitbucket.org/cj4000/stroboskop/commits/0ebcc6e420cd1761287a4aade88bdbf70f289558